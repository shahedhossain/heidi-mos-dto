package biz.shahed.freelance.heidi.mos.pojo.rest;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Digits;

import biz.shahed.freelance.heidi.mos.sencha.data.SenchaDataField;
import biz.shahed.freelance.heidi.mos.sencha.data.SenchaField;
import biz.shahed.freelance.heidi.mos.validator.Name;

public class CityDto implements Serializable {

	private static final long serialVersionUID = -8143802299669915250L;

	@SenchaDataField(idProperty=true)
	@Digits(integer = 8, fraction=0 , message = "Integer required")
	private Integer id;

	@SenchaDataField
	@Name(message="Name required")
	private String name;
	
	@SenchaDataField(type = SenchaField.DATE)
	private Date created;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
}
